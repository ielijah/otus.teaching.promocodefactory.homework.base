﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }
        /// <summary>
        /// Создает новую или обновляет существующую запись
        /// </summary>
        /// <param name="item">экземпляр для добавлекния или обновления</param>
        /// <returns>Возвращает переданное в параметре, если всё ок</returns>
        /// <exception cref="ArgumentNullException">Взрывается, если передали null</exception>
        public Task<T> CreateOrUpdateAsync(T item)
        {
            if(item is null)
                throw new ArgumentNullException(nameof(item));

            if(item.Id == Guid.Empty)
                Create(item);
            else
                Update(item);

            return Task.FromResult(item);
        }
        /// <summary>
        /// Удаляет запись по Id
        /// </summary>
        /// <param name="id">Id записи для удаления</param>
        /// <returns>Возвращает удаленную запись</returns>
        public async Task<T> DeleteAsync(Guid id)
        {
            var item = Data.FirstOrDefault(x => x.Id == id);
            Data = Data.Where(x => x.Id != id);
            return await Task.FromResult(item);
        }
        /// <summary>
        /// Создает новую запись </summary>
        /// <param name="item">новая запись</param>
        private void Create(T item)
        {
            item.Id = Guid.NewGuid();
            Data = Data.Concat(new[] { item });
        }
        /// <summary>
        /// Обновляет существующую запись
        /// </summary>
        /// <param name="item">существующая запись</param>
        private void Update(T item)
        {
            var eItem = Data.FirstOrDefault(x => x.Id == item.Id);
            if (eItem != null)
                eItem.Copy(item);
        }
    }
}
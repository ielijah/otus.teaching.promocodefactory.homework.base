﻿using System;
using System.IO;
using System.Text.Json;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain
{
	public abstract class BaseEntity
	{
		public Guid Id { get; set; }
		public abstract void Copy(BaseEntity item);
	}
}
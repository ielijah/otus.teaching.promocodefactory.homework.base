﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Employee
        : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        public List<Role> Roles { get; set; }

        public int AppliedPromocodesCount { get; set; }

        public override void Copy(BaseEntity item)
        {
            this.FirstName = ((Employee)item).FirstName;
            this.LastName = ((Employee)item).LastName;
            this.Email = ((Employee)item).Email;
            this.AppliedPromocodesCount = ((Employee)item).AppliedPromocodesCount;
            this.Roles = ((Employee)item).Roles;
        }
    }
}
﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Role
        : BaseEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }


        public override void Copy(BaseEntity item)
        {
            this.Name = ((Role)item).Name;
            this.Description = ((Role)item).Description;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles?.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                })?.ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Создать новую запись о сотруднике
        /// </summary>
        /// <param name="firstName">Имя</param>
        /// <param name="lastName">Фамилия</param>
        /// <param name="email">Электронная почта</param>
        /// <returns>Id новой записи о сотруднике</returns>
        [HttpPost]
        public async Task<ActionResult<Guid>> CreateEmployee(string firstName, string lastName, string email)
        {
            Employee employee = new Employee()
            { 
                FirstName = firstName,
                LastName = lastName,
                Email = email
            };

            await _employeeRepository.CreateOrUpdateAsync(employee);

            return Ok(employee.Id);
        }
        /// <summary>
        /// Обновить запись о сотруднике
        /// </summary>
        /// <param name="id">Id записи о сотруднике</param>
        /// <param name="firstName">Имя</param>
        /// <param name="lastName">Фамилия</param>
        /// <param name="email">Электронная почта</param>
        /// <returns>Обновленная запись о сотруднике</returns>
        [HttpPut]
        public async Task<ActionResult<EmployeeResponse>> UpdateEmployee(Guid id, string firstName, string lastName, string email)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();


            employee = new Employee()
            {
                Id = id,
                FirstName = firstName,
                LastName = lastName,
                Email = email
            };
            await _employeeRepository.CreateOrUpdateAsync(employee);

            return Ok(employee);
        }
        /// <summary>
        /// Удалить запись о сотруднике
        /// </summary>
        /// <param name="id">Идентификатор записи для удаления</param>
        /// <returns>Удаленный сотрудник</returns>
        [HttpDelete]
        public async Task<ActionResult<EmployeeShortResponse>> DeleteEmployee(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            employee = await _employeeRepository.DeleteAsync(id);
            var EmployeeShortResponse = new EmployeeShortResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                FullName = employee.FullName
            };

            return Ok(EmployeeShortResponse);
        }
    }
}